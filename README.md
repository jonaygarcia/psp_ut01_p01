# Practica: Tomcat

## Enunciado

Crear una aplicación que tenga un menú que permita realizar las siguientes tareas:

* Arrancar un servidor Tomcat.
* Parar un servidor Tomcat.
* Comprobar el estado de un servidor Tomcat.

Para comprobar el estado del Tomcat hacer uso de la librería [java.net.HttpURLConnection](https://docs.oracle.com/javase/8/docs/api/java/net/HttpURLConnection.html).

Para la realización de esta práctica se recomienda hacer primero los ejercicios 1 y 2 de la Unidad de Trabajo 1.
